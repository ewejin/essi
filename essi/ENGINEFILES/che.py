"""Chemical Equilibrium"""
"""Michael Sansoni"""
"""December 2013"""
"""Method follows equilibrium combustion products"""
"""Ferguson, C.R. 1986"""

import numpy
import fc
import air_data
import fuels

C_aw = 12.01
H_aw = 1.008
O_aw = 16.0
N_aw = 14.0
R_universal = 8314.34
W = 0.21
X = 0.79

def chemical_equilibrium(P, T, phi, xb, air, fuel):
    y, y_fuel, M_weight, R, cp, h, u, v, s = fc.f_c(P, T, phi, xb, air, fuel)
    gas_data_l, gas_data_h = air_data.air(air)
    alpha, beta, gamma, delta, fuel_data = fuels.fuel_data(fuel)
    if T<1000:
        gas_data = gas_data_l
    else:
        gas_data = gas_data_h

    Mf = (alpha*C_aw + beta*H_aw + gamma*O_aw + delta*N_aw)
    M_CO2 = (C_aw + 2*O_aw)
    M_H2O = (2*H_aw + O_aw)
    M_N2 = (2*N_aw)
    M_O2 = (2*O_aw)
    M_CO = (C_aw + O_aw)
    M_H2 = (2*H_aw)
    M_H = (H_aw)
    M_O = (O_aw)
    M_OH = (H_aw + O_aw)
    M_NO = (N_aw + O_aw)

    M_vals = [M_CO2, M_H2O, M_N2, M_O2, M_CO, M_H2, M_H, M_O, M_OH, M_NO]

        
    eps = W / (alpha + beta/4 - gamma/2)
    minmolfrac = 1e-25
    tol = 3e-12

    """Borman and Olikara coefficients"""
    A1 = 0.432168E0
    A2 = 0.310805E0
    A3 = -0.141784E0
    A4 = 0.150879E-01
    A5 = -0.752364E0
    A6 = -0.415302E-02

    B1 = -0.112464E05
    B2 = -0.129540E05
    B3 = -0.213308E04
    B4 = -0.470959E04
    B5 = 0.124210E05
    B6 = 0.148627E05

    C1 = 0.267269E01
    C2 = 0.321779E01
    C3 = 0.853461E0
    C4 = 0.646096E0
    C5 = -0.260286E01
    C6 = -0.475746E01

    D1 = -0.745744E-04
    D2 = -0.738336E-04
    D3 = 0.355015E-04
    D4 = 0.272805E-05
    D5 = 0.259556E-03
    D6 = 0.124699E-03

    E1 = 0.242484E-08
    E2 = 0.344645E-08
    E3 = -0.310227E-08
    E4 = -0.154444E-08
    E5 = -0.162687E-07
    E6 = -0.900227E-08

    Kp = numpy.array([[A1, B1, C1, D1, E1],[A2, B2, C2, D2, E2],[A3, B3, C3, D3, E3],[A4, B4, C4, D4, E4],[A5, B5, C5, D5, E5],[A6, B6, C6, D6, E6]])
    TKp=[numpy.log(T/1000), 1/T, 1, T, T**2]
    K1=10**(A1*numpy.log(T/1000)+B1/T+C1+D1*T+E1*T**2)
    K2=10**(A2*numpy.log(T/1000)+B2/T+C2+D2*T+E2*T**2)
    K3=10**(A3*numpy.log(T/1000)+B3/T+C3+D3*T+E3*T**2)
    K4=10**(A4*numpy.log(T/1000)+B4/T+C4+D4*T+E4*T**2)
    K5=10**(A5*numpy.log(T/1000)+B5/T+C5+D5*T+E5*T**2)
    K6=10**(A6*numpy.log(T/1000)+B6/T+C6+D6*T+E6*T**2)
    K = [K1, K2, K3, K4, K5, K6]

    """Mass balance all equations are equal to zero"""
    #eps*phi*alpha - ((y1+y5)*N)
    #eps*phi*beta - ((2*y2+2*y6+y7+y9)*N)
    #(eps*phi*gamma + 0.42) - ((2*y1+y2+2*y4+y5+y8+y9+y10)*N)
    #(eps*phi*delta + 1.58) - ((2*y3+y10)*N)

    #(y1+y2+y3+y4+y5+y6+y7+y8+y9+y10) - 1

    """Equilibrium constants to molar fractions all equations are equal to zero"""
    #K1 - ((y7 * P**0.5)/y6**0.5)
    #K2 - ((y8 * P**0.5)/y4**0.5)
    #K3 - (y9/(y4**0.5 * y6**0.5))
    #K4 - (y10/(y4**0.5*y3**0.5))
    #K5 - (y2/(y4**0.5 * y6 * P**0.5))
    #K6 - (y1/(y5*y4**0.5*P**0.5))


    P_atm=P/101325

    table = [1/numpy.sqrt(P_atm), 1/numpy.sqrt(P_atm), 1, 1, numpy.sqrt(P_atm), numpy.sqrt(P_atm)]
    c = [0] * len(K)
    dcdT=[0] * 6
    dcdP=[0] * 6
    dfdT=numpy.zeros(4)
    dfdp=numpy.zeros(4)
    dYdT=numpy.zeros(10)
    dYdP=numpy.zeros(10)
    B=numpy.zeros(4)
    for i in range(4):
        y.append(minmolfrac)

    for i, j in enumerate(c):
        c[i] = K[i] * table[i]
    d = [beta/alpha, (gamma+0.42/eps/phi)/alpha, (delta+1.58/eps/phi)/alpha]

    if abs(phi-1)<0.01:
        phi = 0.99

    for i, j in enumerate(y):
        if y[i]<minmolfrac:
            y[i] = minmolfrac

    DY3to6 = numpy.zeros(4)
    for i, j in enumerate(DY3to6):
        DY3to6[i] = 2*tol

    Max_iter = 500.0
    Max_val = 2*tol
    Iter=0.0
    DoneSome=0.0

    while (Iter<Max_iter) and ((Max_val>tol)|(DoneSome<1)):
        Iter = Iter + 1
        if Iter>2:
            DoneSome=1

        D76 = 0.5*c[0]/numpy.sqrt(y[5])
        D84=0.5*c[1]/numpy.sqrt(y[3])
        D94=0.5*c[2]*numpy.sqrt(y[5]/y[3])
        D96=0.5*c[2]*numpy.sqrt(y[3]/y[5])
        D103=0.5*c[3]*numpy.sqrt(y[3]/y[2])
        D104=0.5*c[3]*numpy.sqrt(y[2]/y[3])
        D24=0.5*c[4]*y[5]/numpy.sqrt(y[3])
        D26=c[4]*numpy.sqrt(y[3])
        D14=0.5*c[5]*y[4]/numpy.sqrt(y[3])
        D15=c[5]*numpy.sqrt(y[3])

        
        A = numpy.zeros((4,4))
        A[0,0]=1+D103
        A[0,1]=D14+D24+1+D84+D104+D94
        A[0,2]=D15+1
        A[0,3]=D26+1+D76+D96
        A[1,0]=0
        A[1,1]=2*D24+D94-d[0]*D14
        A[1,2]=-d[0]*D15-d[0]
        A[1,3]=2*D26+2+D76+D96
        A[2,0]=D103
        A[2,1]=2*D14+D24+2+D84+D94+D104-d[1]*D14
        A[2,2]=2*D15+1-d[1]*D15-d[1]
        A[2,3]=D26+D96
        A[3,0]=2+D103
        A[3,1]=D104-d[2]*D14
        A[3,2]=-d[2]*D15-d[2]
        A[3,3]=0
        B[0]=-(sum(y)-1)
        B[1]=-(2*y[1]+2*y[5]+y[6]+y[8]-d[0]*y[0]-d[0]*y[4])
        B[2]=-(2*y[0]+y[1]+2*y[3]+y[4]+y[7]+y[8]+y[9]-d[1]*y[0]-d[1]*y[4])
        B[3]=-(2*y[2]+y[9]-d[2]*y[0]-d[2]*y[4])
        invA = numpy.linalg.inv(A)
        DY3to6=numpy.dot(invA,B)
        Max_vals=abs(DY3to6)
        Max_val = Max_vals.max()
        
        for i in range(4):
            y[i+2]=y[i+2]+DY3to6[i]/10
        for i, j in enumerate(y):
            if y[i]<minmolfrac:
                y[i] = minmolfrac
                
        y[6]=c[0]*numpy.sqrt(y[5])
        y[7]=c[1]*numpy.sqrt(y[3])
        y[8]=c[2]*numpy.sqrt(y[3]*y[5])
        y[9]=c[3]*numpy.sqrt(y[3]*y[2])
        y[1]=c[4]*numpy.sqrt(y[3])*y[5]
        y[0]=c[5]*numpy.sqrt(y[3])*y[4]

    if Iter>Max_iter:
        print "No convergence in solution"

    TdKdT=[1/T, -1/T**2, 0, 1, 2*T]
    dKdT1=numpy.dot(Kp,TdKdT)
    dKdT = [0] * len(dKdT1)
    for i, j in enumerate(dKdT1):
        dKdT[i] = 2.302585*K[i]*dKdT1[i]
    dcdT[0]=dKdT[0]/numpy.sqrt(P_atm)
    dcdT[1]=dKdT[1]/numpy.sqrt(P_atm)
    dcdT[2]=dKdT[2]
    dcdT[3]=dKdT[3]

    dcdT[4]=dKdT[4]*numpy.sqrt(P_atm)
    dcdT[5]=dKdT[5]*numpy.sqrt(P_atm)
    dcdP[0]=-0.5*c[0]/P
    dcdP[1]=-0.5*c[1]/P
    dcdP[4]=0.5*c[4]/P
    dcdP[5]=0.5*c[5]/P

    x1=y[0]/c[5]
    x2=y[1]/c[4]
    x7=y[6]/c[0]
    x8=y[7]/c[1]
    x9=y[8]/c[2]
    x10=y[9]/c[3]

    dfdT[0]=dcdT[5]*x1+dcdT[4]*x2+dcdT[0]*x7+dcdT[1]*x8+dcdT[2]*x9+dcdT[3]*x10
    dfdT[1]=2*dcdT[4]*x2+dcdT[0]*x7+dcdT[2]*x9-d[0]*dcdT[5]*x1
    dfdT[2]=2*dcdT[5]*x1+dcdT[4]*x2+dcdT[1]*x8+dcdT[2]*x9+dcdT[3]*x10-d[1]*dcdT[5]*x1
    dfdT[3]=dcdT[3]*x10-d[2]*dcdT[5]*x1
    dfdp[0]=dcdP[5]*x1+dcdP[4]*x2+dcdP[0]*x7+dcdP[1]*x8
    dfdp[1]=2*dcdP[4]*x2+dcdP[0]*x7-d[0]*dcdP[5]*x1
    dfdp[2]=2*dcdP[5]*x1+dcdP[4]*x2+dcdP[1]*x8-d[1]*dcdP[5]*x1
    dfdp[3]=-d[2]*dcdP[5]*x1

    B=-dfdT
    dYdT[2:6]=numpy.dot(invA,B)
    dYdT[0]=numpy.sqrt(y[3])*y[4]*dcdT[5]+D14*dYdT[3]+D15*dYdT[4]
    dYdT[1]=numpy.sqrt(y[3])*y[5]*dcdT[4]+D24*dYdT[3]+D26*dYdT[5]
    dYdT[6]=numpy.sqrt(y[5])*dcdT[0]+D76*dYdT[5]
    dYdT[7]=numpy.sqrt(y[3])*dcdT[1]+D84*dYdT[3]
    dYdT[8]=numpy.sqrt(y[3]*y[5])*dcdT[2]+D94*dYdT[3]+D96*dYdT[5]
    dYdT[9]=numpy.sqrt(y[3]*y[2])*dcdT[3]+D104*dYdT[3]+D103*dYdT[2]

    B=-dfdp
    dYdP[2:6]=numpy.dot(invA,B)
    dYdP[0]=numpy.sqrt(y[3])*y[4]*dcdP[5]+D14*dYdP[3]+D15*dYdP[4]
    dYdP[1]=numpy.sqrt(y[3])*y[5]*dcdP[4]+D24*dYdP[3]+D26*dYdP[5]
    dYdP[6]=numpy.sqrt(y[5])*dcdP[0]+D76*dYdP[5]
    dYdP[7]=numpy.sqrt(y[3])*dcdP[1]+D84*dYdP[3]
    dYdP[8]=D94*dYdP[3]+D96*dYdP[5]
    dYdP[9]=D104*dYdP[3]+D103*dYdP[2]

    Tcp0=[1, T, T**2, T**3, T**4]
    Th0=[1, T/2, T**2/3, T**3/4, T**4/5, 1/T]
    Ts0=[numpy.log(T), T, T**2/2, T**3/3, T**4/4, 0, 1]
    cp0=numpy.dot(gas_data[:,0:5],Tcp0)
    h0=numpy.dot(gas_data[:,0:6],Th0)
    s0=numpy.dot(gas_data[:,0:7],Ts0)

    y[0]=(2*y[2]+y[9])/d[2]-y[4]
    y[1]=(d[0]/d[2]*(2*y[2]+y[9])-2*y[5]-y[6]-y[8])/2

    for i, j in enumerate(y):
        if y[i]<minmolfrac:
            y[i] = minmolfrac

    M_weight=numpy.dot(y,M_vals)
    R=R_universal/M_weight
    h=R * T * (numpy.dot(h0,y))

    s=R*(-numpy.log(P_atm)+numpy.dot((s0-numpy.log(y)),y))
    cp1 = numpy.dot(y,cp0)
    cp2 = numpy.dot(h0,dYdT*T)
    cp = (cp1 + cp2)
    MT=numpy.dot(dYdT,M_vals)
    Mp=numpy.dot(dYdP,M_vals)
    v=R*T/P
    cp = R*(cp-(numpy.dot(h0,y))*T*MT/M_weight)
    dlvlT=1+max(-T*MT/M_weight,0)
    dlvlp=-1-max(P*Mp/M_weight,0)
    u=h-R*T

    return y, cp, R, M_weight, h, u, v, s, dlvlT, dlvlp
