"""Engine Model"""
"""Version 3"""
"""Michael Sansoni"""
"""10th December 2013"""

""" ************************************************************************************ """
""" ********************************IMPORT PACKAGES************************************* """
""" ************************************************************************************ """
import numpy
import math
import scipy.integrate as si
import scipy.interpolate as sii
import matplotlib.pyplot as plt

""" ************************************************************************************ """
""" ********************************IMPORT FUNCTIONS************************************ """
""" ************************************************************************************ """
import adiabatic_flame
import air_data
import che
import fc
import fuels
import mfb_model
import valve_data
import volume_model

""" ************************************************************************************ """
""" ********************************DEFINE ENGINE CLASS********************************* """
""" ************************************************************************************ """
class EngineSimulator():
    def __init__(self, parent=None):
        self.air = "GMcB"
        self.fuel = "gasoline"
        self.heat_transfer = 'constant'
        self.burn_model = 'cosine model'
        self.dissociation_model = 'none'
        
        """Gas constants"""
        self.R_universal = 8314.34
        self.C_aw = 12.011
        self.H_aw = 1.008
        self.O_aw = 16.0
        self.N_aw = 14.008

        """Engine parameters"""
        self.compression_ratio = 10.0
        self.crank_angle_ignition = -35.0*numpy.pi/180
        self.burn_duration = 60.0*numpy.pi/180
        self.b = 0.1
        self.stroke = 0.08
        self.eps = 0.25
        self.e_rpm = 2000.0
        self.Cb = 0.8

        """Exhaust Valve Data"""
        self.evo = numpy.pi                       
        self.evc = 2*numpy.pi                                    
        self.epd = 0.04                                       
        self.esd = 0.015                                          
        self.el_max = 0.035                                             

        """Intake Valve Data"""
        self.ivo = self.evc                                        
        self.ivc = 3*numpy.pi                                     
        self.ipd = 0.04                                           
        self.isd = 0.015                                          
        self.il_max = 0.035

        self.P_int = 100000.0
        self.P_exh = 150000.0

        """Fuel air parameters"""
        self.xr = 0.1
        self.phi = 0.8

        """Heat transfer parameters"""
        self.hcoeff_u = 500.0
        self.hcoeff_b = 500.0
        self.hcoeff_weighting = 1.0
        
        """Initial conditions"""
        self.T1 = 350.0
        self.Tw = 420.0
        self.P_atm = 101325.0 #Atmospheric pressure pa

        self.initial_calculations()

    def initial_calculations(self):
        """Calculates some of the initial conditions"""
        self.P1 = self.P_int
        self.volume_TDC = (numpy.pi*self.b**2/4)*self.stroke/(self.compression_ratio-1)
        self.volume_BDC = (numpy.pi*self.b**2/4)*self.stroke+self.volume_TDC
        
        """Derived constants"""
        self.xb = self.xr
        self.omega = (self.e_rpm*2*numpy.pi)/60.0

        """Calculate initial conditions"""
        self.y1, self.y_fuel1, self.M_weight1, self.R1, self.cp1, self.h1, self.u1, self.v1, self.s1 = fc.f_c(self.P1, self.T1, self.phi, self.xb, self.air, self.fuel)
        self.V1 = self.volume_BDC
        self.m1 = self.V1/self.v1
        self.U1 = self.u1*self.m1
        self.mf = self.y_fuel1*self.m1
        self.crank_angle = []
        self.theta1 = -numpy.pi
        self.dtheta = 1.0*numpy.pi/180
        if self.dissociation_model == 'full':
            self.diss_switch = 1000.0
        else:
            self.diss_switch = 5000.0
        if self.burn_model == 'instant model':
            self.crank_angle_ignition = 0.0
            self.burn_duration = 0.1

    def df_comp(self, inputs, theta):
        """Derivative equation to calculate the compression stroke"""
        """Returns derivatives of Pressure, Temperature, Work, Heat Transfer"""
        """Heat Leakage with respect to crank angle"""
        """Equations from: C. R. Ferguson, Internal Combustion Engines"""
        p=inputs[0]
        Tu=inputs[1]
        m=self.m1*numpy.exp(-self.Cb*(theta-self.theta1)/self.omega) #Calculates new mass in cylinder accounting for blowby
        V, DV = volume_model.volume_model(theta, self.volume_TDC, self.eps, self.compression_ratio)
        """Calculation of heat transfer coefficient"""
        if self.heat_transfer=='woschni':
            u_p=self.omega*self.stroke/numpy.pi
            C1=2.28
            hcoeff=self.hcoeff_weighting*130*self.b**(-0.2)*Tu**(-0.53)*(p/self.P_atm)**(0.8)*C1*u_p
        else:
            hcoeff = self.hcoeff_u
        Qconv=hcoeff*(numpy.pi*self.b**2/2+4*V/self.b)*(Tu-self.Tw)
        y, y_fuel,M_weight, R, cp, h, u, v, s = fc.f_c(p, Tu, self.phi, self.xb, self.air, self.fuel)
        dlvlT = 1.0
        dlvlp = -1.0
        f1=1.0/m*(DV+V*self.Cb/self.omega)
        f2=(1.0/(self.omega*m))*v/cp*dlvlT*Qconv/Tu
        f3=0
        f4=0
        f5=v**2/cp/Tu*dlvlT**2+v/p*dlvlp
        DP=(f1+f2+f3)/(f4+f5)
        DTu=-(1.0/(self.omega*m))/cp*Qconv+v/cp*dlvlT*DP
        DW=p*DV
        DQ=Qconv/self.omega
        DH=self.Cb*m/self.omega*h
        return [DP, DTu, DW, DQ, DH]

    def df_comb(self, inputs, theta):
        """Derivative equation to calculate the combustion phase"""
        """Returns derivatives of Pressure, Temperature(burned/unburned), Work"""
        """ Heat Transfer and Heat Leakage with respect to crank angle"""
        """Equations from: C. R. Ferguson, Internal Combustion Engines"""
        p=inputs[0]
        Tb=inputs[1]
        Tu=inputs[2]
        m=self.m1*numpy.exp(-self.Cb*(theta-self.theta1)/self.omega)
        V, DV = volume_model.volume_model(theta, self.volume_TDC, self.eps, self.compression_ratio)
        if self.burn_model=='weibe model':
            X, DX = mfb_model.weibe_model(theta, self.xb, self.crank_angle_ignition, self.burn_duration)
        elif self.burn_model=='cosine model':
            X, DX = mfb_model.cosine_model(theta, self.xb, self.crank_angle_ignition, self.burn_duration)
        else:
            X, DX = mfb_model.cosine_model(theta, self.xb, self.crank_angle_ignition, self.burn_duration)
        if self.heat_transfer=='woschni':
            u_p=self.omega*self.stroke/numpy.pi
            C1=2.28
            C2=3.24e-3
            V_swept=self.volume_BDC-self.volume_TDC
            k=1.3
            pm=self.P1*(self.volume_BDC/V)**k
            hcoeffu=self.hcoeff_weighting*130*self.b**(-0.2)*Tu**(-0.53)*(p/self.P_atm)**(0.8)*(C1*u_p+C2*V_swept*self.T1/self.P1/self.volume_BDC*(p-pm))**(0.8)
            hcoeffb=self.hcoeff_weighting*130*self.b**(-0.2)*Tb**(-0.53)*(p/self.P_atm)**(0.8)*(C1*u_p+C2*V_swept*self.T1/self.P1/self.volume_BDC*(p-pm))**(0.8)
        else:
            hcoeffu=self.hcoeff_u
            hcoeffb=self.hcoeff_b
        Qconvu=hcoeffu*(numpy.pi*self.b**2/2+4*V/self.b)*(1-numpy.sqrt(X))*(Tu-self.Tw)
        Qconvb=hcoeffb*(numpy.pi*self.b**2/2+4*V/self.b)*numpy.sqrt(X)*(Tb-self.Tw)
        yu, y_fuelu, M_weightu, Ru, cpu, hu, uu, vu, su = fc.f_c(p, Tu, self.phi, self.xb, self.air, self.fuel)
        if Tb>self.diss_switch:
            yb, cpb, Rb, M_weightb, hb, ub, vb, sb, dlvlTb, dlvlpb = che.chemical_equilibrium(p, Tb, self.phi, X, self.air, self.fuel)
        else:
            yb, y_fuelb, M_weightb, Rb, cpb, hb, ub, vb, sb = fc.f_c(p, Tb, self.phi, 1, self.air, self.fuel)
            dlvlTb = 1.0
            dlvlpb = -1.0
        dlvlTu = 1.0
        dlvlpu = -1.0
        f1=1.0/m*(DV+V*self.Cb/self.omega)
        f2=(1.0/(self.omega*m))*(vb/cpb*dlvlTb*Qconvb/Tb+vu/cpu*dlvlTu*Qconvu/Tu)
        f3=-(vb-vu)*DX-vb*dlvlTb*(hu-hb)/cpb/Tb*(DX-(X-X**2)*self.Cb/self.omega)
        f4=X*(vb**2/cpb/Tb*dlvlTb**2+vb/p*dlvlpb)
        f5=(1-X)*(vu**2/cpu/Tu*dlvlTu**2+vu/p*dlvlpu)
        DP=(f1+f2+f3)/(f4+f5)
        DTb=-(1.0/(self.omega*m))/cpb/X*Qconvb+vb/cpb*dlvlTb*DP+(hu-hb)/cpb*(DX/X-(1-X)*self.Cb/self.omega)
        DTu=-(1.0/(self.omega*m))/cpu/(1-X)*Qconvu+vu/cpu*dlvlTu*DP
        DW=p*DV
        DQ=(1.0/self.omega)*(Qconvb+Qconvu)
        DH=self.Cb*m/self.omega*((1-X**2)*hu+X**2*hb)
        return [DP, DTb, DTu, DW, DQ, DH]

    def df_exp(self, inputs, theta):
        """Derivative equation to calculate the expansiion stroke"""
        """Returns derivatives of Pressure, Temperature, Work, Heat Transfer"""
        """Heat Leakage with respect to crank angle"""
        """Equations from: C. R. Ferguson, Internal Combustion Engines"""
        p=inputs[0]
        Tb=inputs[1]
        m=self.m1*numpy.exp(-self.Cb*(theta-self.theta1)/self.omega)
        V, DV = volume_model.volume_model(theta, self.volume_TDC, self.eps, self.compression_ratio)
        if self.heat_transfer=='woschni':
            u_p=self.omega*self.stroke/numpy.pi
            C1=2.28
            hcoeff=self.hcoeff_weighting*130*self.b**(-0.2)*Tb**(-0.53)*(p/self.P_atm)**(0.8)*C1*u_p
        else:
            hcoeff = self.hcoeff_b
        Qconv=hcoeff*(numpy.pi*self.b**2/2+4*V/self.b)*(Tb-self.Tw)
        if Tb<self.diss_switch:
            y, y_fuel, M_weight, R, cp, h, u, v, s = fc.f_c(p, Tb, self.phi, 1.0, self.air, self.fuel)
            dlvlT = 1.0
            dlvlp = -1.0
        else:
            y, cp, R, M_weight, h, u, v, s, dlvlT, dlvlp = che.chemical_equilibrium(p, Tb, self.phi, 1.0, self.air, self.fuel)
        f1=1.0/m*(DV+V*self.Cb/self.omega)
        f2=(1.0/(self.omega*m))*v/cp*dlvlT*Qconv/Tb
        f3=0.0
        f4=v**2/cp/Tb*dlvlT**2+v/p*dlvlp
        f5=0.0
        DP=(f1+f2+f3)/(f4+f5)
        DTb=-(1.0/(self.omega*m))/cp*Qconv+v/cp*dlvlT*DP
        DW=p*DV
        DQ=Qconv/self.omega
        DH=self.Cb*m/self.omega*h
        return [DP, DTb, DW, DQ, DH]

    def comp(self):
        """Iterates over the compression stroke"""
        no_its = math.ceil((self.crank_angle_ignition-(-numpy.pi))/self.dtheta)
        self.crank_angle_comp = numpy.linspace(-numpy.pi,self.crank_angle_ignition,no_its)
        self.PTWQH_comp = si.odeint(self.df_comp, [self.P1, self.T1, 0, 0, 0], self.crank_angle_comp)

        """Interpolate results to obtain B.Cs for combustion phase"""
        P_comp = sii.interp1d(self.crank_angle_comp,self.PTWQH_comp[:,0])
        self.Pb = P_comp(self.crank_angle_ignition)
        Tu_comp = sii.interp1d(self.crank_angle_comp,self.PTWQH_comp[:,1])
        self.Tub = Tu_comp(self.crank_angle_ignition)
        W_comp = sii.interp1d(self.crank_angle_comp,self.PTWQH_comp[:,2])
        self.Wb = W_comp(self.crank_angle_ignition)
        Q_comp = sii.interp1d(self.crank_angle_comp,self.PTWQH_comp[:,3])
        self.Qb = Q_comp(self.crank_angle_ignition)
        H_comp = sii.interp1d(self.crank_angle_comp,self.PTWQH_comp[:,4])
        self.Hb = H_comp(self.crank_angle_ignition)

    def comb(self):
        """Iterates over the combustion stroke"""
        self.Tb, h_ft, u_ft, v_ft = adiabatic_flame.adiabatic_flame(self.Pb, self.Tub, self.phi, self.xb, self.air, self.fuel)
        no_its = math.ceil(((self.crank_angle_ignition+self.burn_duration)-self.crank_angle_ignition)/self.dtheta)
        self.crank_angle_comb = numpy.linspace(self.crank_angle_ignition,self.crank_angle_ignition+self.burn_duration,no_its)
        self.PTWQH_comb = si.odeint(self.df_comb, [self.Pb, self.Tb, self.Tub, self.Wb, self.Qb, self.Hb], self.crank_angle_comb)

        """Interpolate results to obtain B.Cs for expansion phase"""
        P_comb = sii.interp1d(self.crank_angle_comb,self.PTWQH_comb[:,0])
        self.Pe = P_comb((self.crank_angle_ignition+self.burn_duration))
        Tb_comb = sii.interp1d(self.crank_angle_comb,self.PTWQH_comb[:,1])
        self.Te = Tb_comb((self.crank_angle_ignition+self.burn_duration))
        W_comb = sii.interp1d(self.crank_angle_comb,self.PTWQH_comb[:,3])
        self.We = W_comb((self.crank_angle_ignition+self.burn_duration))
        Q_comb = sii.interp1d(self.crank_angle_comb,self.PTWQH_comb[:,4])
        self.Qe = Q_comb((self.crank_angle_ignition+self.burn_duration))
        H_comb = sii.interp1d(self.crank_angle_comb,self.PTWQH_comb[:,5])
        self.He = H_comb((self.crank_angle_ignition+self.burn_duration))

    def exp(self):
        """Iterates over the expansion"""
        no_its = math.ceil((self.evo-(self.crank_angle_ignition+self.burn_duration))/self.dtheta)
        self.crank_angle_exp = numpy.linspace(self.crank_angle_ignition+self.burn_duration,self.evo,no_its)
        self.PTWQH_exp = si.odeint(self.df_exp, [self.Pe, self.Te, self.We, self.Qe, self.He], self.crank_angle_exp)

        """Interpolate results to obtain final constants"""
        P_exp = sii.interp1d(self.crank_angle_exp,self.PTWQH_exp[:,0])
        self.P4 = P_exp(self.evo)
        Tb_exp = sii.interp1d(self.crank_angle_exp,self.PTWQH_exp[:,1])
        self.T4 = Tb_exp(self.evo)
        W_exp = sii.interp1d(self.crank_angle_exp,self.PTWQH_exp[:,2])
        self.W4 = W_exp(self.evo)
        Q_exp = sii.interp1d(self.crank_angle_exp,self.PTWQH_exp[:,3])
        self.Q4 = Q_exp(self.evo)
        H_exp = sii.interp1d(self.crank_angle_exp,self.PTWQH_exp[:,4])
        self.H4 = H_exp(self.evo)

    def exh(self):
        """Iterates over the expansion"""
        self.N = 100
        P_exh = [0] * self.N
        T_exh = [0] * self.N
        W_exh = [0] * self.N
        m_exh = [0] * self.N
        dth = (self.evc - self.evo)/self.N
        crank_angle = [0] * self.N
        crank_angle[0] = self.evo
        P_exh[0] = self.P4
        T_exh[0] = self.T4
        W_exh[0] = self.W4
        m_exh[0] = self.m4
        Pe = self.P_exh
        for i in range(1,self.N):
            y, y_fuel, M_weight, R, cp, h, u, v, s = fc.f_c(P_exh[i-1], T_exh[i-1], self.phi, 1.0, self.air, self.fuel)
            k = cp/(cp - R)
            crank_angle[i] = crank_angle[i-1] + dth                                   #create new angle of iteration
            Ae = valve_data.valve_data(crank_angle[i-1],self.evo,self.evc,self.epd,self.esd,self.el_max) #calculate the exhaust valve opening and closing
            mf = valve_data.mass_flow(P_exh[i-1],T_exh[i-1],Pe,k,R,Ae,self.omega)            #calculate the mass flow in and out the valve
            V, DV = volume_model.volume_model(crank_angle[i-1], self.volume_TDC, self.eps, self.compression_ratio)
            m_exh[i] = m_exh[i-1] - mf*dth                                #calculate the new mass in the valve
            T_exh[i] = T_exh[i-1] -(k -1) *(DV/V+mf/m_exh[i-1])*T_exh[i-1]*dth    #calculate T for the iteration and add it to the list
            V, DV = volume_model.volume_model(crank_angle[i], self.volume_TDC, self.eps, self.compression_ratio)                                          #add model return to V list
            P_exh[i] = m_exh[i] * R * T_exh[i]/V
            W_exh[i] = W_exh[i-1] + P_exh[i-1]*DV*dth
        self.PTWm_exh = [P_exh, T_exh, W_exh, m_exh]
        self.crank_angle_exh = crank_angle

    def intake(self):
        """Iterates over the expansion"""
        self.pressure_exh = self.PTWm_exh[0]
        self.temperature_exh = self.PTWm_exh[1]
        self.work_exh = self.PTWm_exh[2]
        self.m_exh = self.PTWm_exh[3]
        P_int = [0] * self.N
        T_int = [0] * self.N
        W_int = [0] * self.N
        m_int = [0] * self.N
        self.P5 = self.pressure_exh[self.N-1]
        self.T5 = self.temperature_exh[self.N-1]
        self.W5 = self.work_exh[self.N-1]
        self.m5 = self.m_exh[self.N-1]
        mr = self.m5
        dth = (self.ivc - self.ivo)/self.N
        crank_angle = [0] * self.N
        crank_angle[0] = self.ivo-dth
        P_int[0] = self.P5
        T_int[0] = self.T5
        W_int[0] = self.W5
        m_int[0] = self.m5
        Pi = self.P_int
        for i in range(1,self.N):
            x = mr/m_int[i-1]
            y, y_fuel, M_weight, R, cp, h, u, v, s = fc.f_c(P_int[i-1], T_int[i-1], self.phi, x, self.air, self.fuel)
            k = cp/(cp - R)
            crank_angle[i] = crank_angle[i-1] + dth                                   #create new angle of iteration
            Ai = valve_data.valve_data(crank_angle[i-1],self.ivo,self.ivc,self.ipd,self.isd,self.il_max) #calculate the exhaust valve opening and closing
            mf = valve_data.mass_flow(Pi, T_int[i-1],P_int[i-1],k,R,Ai,self.omega)            #calculate the mass flow in and out the valve
            V, DV = volume_model.volume_model(crank_angle[i-1], self.volume_TDC, self.eps, self.compression_ratio)
            m_int[i] = m_int[i-1] + mf*dth                                #calculate the new mass in the valve
            T_int[i] = T_int[i-1] + (-(k-1)*DV*T_int[i-1]/V+mf*(cp*self.T1-(cp-R)*T_int[i-1])/(m_int[i-1]*(cp-R)))*dth
            V, DV = volume_model.volume_model(crank_angle[i], self.volume_TDC, self.eps, self.compression_ratio)                                          #add model return to V list
            P_int[i] = m_int[i] * R * T_int[i]/V
            W_int[i] = W_int[i-1] + P_int[i-1]*DV*dth
        self.P6 = P_int[self.N-1]
        self.T6 = T_int[self.N-1]
        self.W6 = W_int[self.N-1]
        self.PTWm_int = [P_int, T_int, W_int, m_int]
        self.crank_angle_int = crank_angle
        self.xb_new = x
        y6, y_fuel6, M_weight6, R6, cp6, h6, u6, v6, s6 = fc.f_c(self.P6, self.T6, self.phi, x, self.air, self.fuel)
        
    def errors(self):
        """Check for errors and power stroke performance data"""
        mass4=self.m1*numpy.exp(-self.Cb*2*numpy.pi/self.omega)
        y4, y_fuel4, M_weight4, R4, cp4, h4, u4, v4, s4 = fc.f_c(self.P4, self.T4, self.phi, 1.0, self.air, self.fuel)
        U4 = u4*mass4
        self.error1 = 1 - v4*mass4/self.volume_BDC
        self.error2 = 1 + self.W4/(U4-self.U1+self.Q4+self.H4)
        self.eta=self.W4/self.m1*(1+self.phi*0.06548*(1-self.xb))/self.phi/0.06548/(1-self.xb)/47870/1e3
        self.m4 = mass4
        self.co_exh_frac = y4[4]

    def outputs(self):
        """Create output vectors for graphical display"""
        self.crank_angle_full = numpy.hstack((self.crank_angle_comp, self.crank_angle_comb, self.crank_angle_exp, self.crank_angle_exh, self.crank_angle_int))
        self.crank_angle_half = numpy.hstack((self.crank_angle_comp, self.crank_angle_comb, self.crank_angle_exp))
        self.crank_angle_compcomb = numpy.hstack((self.crank_angle_comp, self.crank_angle_comb))
        self.crank_angle_combexpexhint = numpy.hstack((self.crank_angle_comb, self.crank_angle_exp))#, self.crank_angle_exh, self.crank_angle_int))#
        self.pressure_full = numpy.hstack((self.PTWQH_comp[:,0], self.PTWQH_comb[:,0], self.PTWQH_exp[:,0], self.PTWm_exh[0], self.PTWm_int[0]))
        self.pressure_half = numpy.hstack((self.PTWQH_comp[:,0], self.PTWQH_comb[:,0], self.PTWQH_exp[:,0]))
        self.vol_full = [0] * len(self.crank_angle_full)
        for elem in range(len(self.crank_angle_full)):
            self.vol_full[elem], dv = volume_model.volume_model(self.crank_angle_full[elem], self.volume_TDC, self.eps, self.compression_ratio)
        self.vol_half = [0] * len(self.crank_angle_half)
        for elem in range(len(self.crank_angle_half)):
            self.vol_half[elem], dv = volume_model.volume_model(self.crank_angle_half[elem], self.volume_TDC, self.eps, self.compression_ratio)
        self.temperature_unburned_compcomb = numpy.hstack((self.PTWQH_comp[:,1], self.PTWQH_comb[:,2]))
        self.temperature_burned_combexpexhint = numpy.hstack((self.PTWQH_comb[:,1], self.PTWQH_exp[:,1]))#, self.PTWm_exh[1], self.PTWm_int[1]))
        self.work_half = numpy.hstack((self.PTWQH_comp[:,2], self.PTWQH_comb[:,3], self.PTWQH_exp[:,2]))
        self.work_full = numpy.hstack((self.PTWQH_comp[:,2], self.PTWQH_comb[:,3], self.PTWQH_exp[:,2], self.PTWm_exh[2], self.PTWm_int[2]))
        self.heat_transfer_half = numpy.hstack((self.PTWQH_comp[:,3], self.PTWQH_comb[:,4], self.PTWQH_exp[:,3]))
        self.heat_leakage_half = numpy.hstack((self.PTWQH_comp[:,4], self.PTWQH_comb[:,5], self.PTWQH_exp[:,4]))
        self.imep=self.W6/(numpy.pi*self.b**2/4*self.stroke)/1000
        self.power = self.imep*(self.e_rpm/60.0)/2.0 #P in Heywood Power output in watt
        self.m_dot_f = (self.e_rpm*60) * self.mf
        self.sfc = self.m_dot_f/(self.power/1000)
        
    def run(self):
        self.initial_calculations()
        self.comp()
        self.comb()
        self.exp()
        self.errors()
        self.exh()
        self.intake()
        self.outputs()
