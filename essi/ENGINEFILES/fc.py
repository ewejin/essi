"""Frozen composition"""
"""Fuel gas calculations"""
"""Based on literature by Ferguson, C.R, 1986"""
"""and Buttsworth 2001"""
"""Michael Sansoni"""
"""December 2013"""

import numpy
import air_data
import fuels

C_aw = 12.01
H_aw = 1.008
O_aw = 16.0
N_aw = 14.0
R_universal = 8314.34
W = 0.21
X = 0.79

def f_c(P, T, phi, xb, air, fuel):
    """in frozen case only evpecting CO2, H2O, N2, O2, CO, H2"""
    gas_data_l, gas_data_h = air_data.air(air)
    alpha, beta, gamma, delta, fuel_data = fuels.fuel_data(fuel)
    Mf = (alpha*C_aw + beta*H_aw + gamma*O_aw + delta*N_aw)
    M_CO2 = (C_aw + 2*O_aw)
    M_H2O = (2*H_aw + O_aw)
    M_N2 = (2*N_aw)
    M_O2 = (2*O_aw)
    M_CO = (C_aw + O_aw)
    M_H2 = (2*H_aw)
    M_vals = [M_CO2, M_H2O, M_N2, M_O2, M_CO, M_H2]
    table=[-1, 1, 0, 0, 1, -1]
    minmolfrac = 1e-25
    eps = W / (alpha + beta/4 - gamma/2)

    """Atom balances"""
    #C: 0 = (phi * eps * alpha) - (v1 + v5)
    #H: 0 = (phi * eps * beta) - (2*v2 + 2*v6)
    #O: 0 = ((phi * eps * gamma) + 2*W) - (2*v1 + v2 + 2*v4 + v5)
    #N: 0 = ((phi * eps * delta) + 2*X) - (2*v3)

    #K: K = (v2*v5)/(v1*v6)

    """Nitrogen"""
    v3 = ((phi * eps * delta) + 2*X)/2

    if phi<=1.0:
        y=[alpha*phi*eps, beta*phi*eps/2, 0.79+delta*phi*eps/2, 0.21*(1-phi), 0, 0]
        dcdT = 0.0

    else:
        z=1000/T
        K=numpy.exp(2.743+z*(-1.761+z*(-1.611+z*0.2803)))
        dKdT=-K*(-1.761+z*(-3.222+z*0.8409))/1000
        a=1-K
        b=0.42-phi*eps*(2*alpha-gamma)+K*(0.42*(phi-1)+alpha*phi*eps)
        c=-0.42*alpha*phi*eps*(phi-1)*K
        nu5=(-b+numpy.sqrt(b**2-4*a*c))/(2*a)
        dcdT=dKdT*(nu5**2-nu5*(0.42*(phi-1)+alpha*phi*eps)+0.42*alpha*phi*eps*(phi-1))/(2*nu5*a+b)
        y=[alpha*phi*eps-nu5, 0.42-phi*eps*(2*alpha-gamma)+nu5, 0.79+delta*phi*eps/2, 0, nu5, 0.42*(phi-1)-nu5]

    tmoles = sum(y)
    for i, j in enumerate(y):
        y[i] = y[i]/tmoles

    M_res = [0] * len(y)
    for i, j in enumerate(y):
        M_res[i] = y[i] * M_vals[i]

    M_residual = sum(M_res)

    yf = (eps * phi)/(1 + eps * phi)
    vo2=0.21/(1+eps*phi)
    vn2=0.79/(1+eps*phi)
    M_fuel_air = (yf * Mf) + (vo2*2*O_aw) + (vn2*2*N_aw)

    y_res = xb/(xb+M_residual/M_fuel_air*(1-xb))
    for i, j in enumerate(y):
        y[i] = y[i] * y_res

    y_fuel = yf * (1 - y_res)
    y[2] = y[2] + vn2*(1-y_res)
    y[3] = y[3] + vo2*(1-y_res)

    if T<1000:
        gas_data = gas_data_l
    else:
        gas_data = gas_data_h
    Tcp0=[1, T, T**2, T**3, T**4]
    Th0=[1, T/2, T**2/3, T**3/4, T**4/5, 1/T]
    Ts0=[numpy.log(T), T, T**2/2, T**3/3, T**4/4, 0, 1]
    cp0=numpy.dot(gas_data[0:6,0:5],Tcp0)
    h0=numpy.dot(gas_data[0:6,0:6],Th0)
    s0=numpy.dot(gas_data[0:6,0:7],Ts0)

    cpfuel=numpy.dot(fuel_data[0:5],[1, T, T**2, T**3, 1/T**2])
    hfuel=numpy.dot(fuel_data[0:6],[1, T/2, T**2/3, T**3/4, -1/T**2, 1/T])
    s0fuel=numpy.dot(fuel_data[0:7],[numpy.log(T), T, T**2/2, T**3/3, -1/T**2/2, 0, 1])

    if y_fuel<minmolfrac:
        y_fuel = minmolfrac

    for i, j in enumerate(y):
        if y[i]<minmolfrac:
            y[i] = minmolfrac

    M_weight=Mf*y_fuel+numpy.dot(y,M_vals)
    R=R_universal/M_weight

    h = R * T * (hfuel*y_fuel+numpy.dot(h0,y))
    u = h - (R * T)
    v = (R * T) /P

    for i, j in enumerate(table):
        table[i] = table[i]*T*dcdT*y_res/tmoles
    cp = R * (cpfuel*y_fuel+numpy.dot(cp0,y)+numpy.dot(h0,table))

    s = R * (-numpy.log(P/101.325e3)+(s0fuel-numpy.log(y_fuel))*y_fuel+numpy.dot((s0-numpy.log(y)),y))
    return y, y_fuel, M_weight, R, cp, h, u, v, s
