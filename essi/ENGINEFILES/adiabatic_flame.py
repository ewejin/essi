"""Adiabatic Flame Temperature"""
"""Michael Sansoni"""
"""December 2013"""
"""Based on Buttsworth, 2001 model"""

import fc
import che
import air_data
import fuels

C_aw = 12.01
H_aw = 1.008
O_aw = 16.0
N_aw = 14.0
R_universal = 8314.34
W = 0.21
X = 0.79

def adiabatic_flame(P, Tu, phi, xb, air, fuel):
    gas_data_l, gas_data_h = air_data.air(air)
    alpha, beta, gamma, delta, fuel_data = fuels.fuel_data(fuel)
    y_u, y_fuel, M_weight, R, cp_u, h_u, u_u, v_u, s_u = fc.f_c(P, Tu, phi, xb, air, fuel)
    no_iterations = 100.0
    tol = 1e-10
    Tb = 2000.0 #Initial guess
    T_delta = 5*tol*Tb
    counter = 0
    while counter<no_iterations and abs(T_delta/Tb)>tol:
        y_b, cp_b, R, M_weight, h_b, u_b, v_b, s_b, dlvlT, dlvlp = che.chemical_equilibrium(P, Tb, phi, xb, air, fuel)
        T_delta = (h_u-h_b)/cp_b
        Tb = Tb + T_delta
        counter = counter + 1
        if counter==no_iterations:
            print "no convergence in solution"
            break
    return Tb, h_b, u_b, v_b
    
