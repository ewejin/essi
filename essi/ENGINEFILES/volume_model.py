"""Volume model"""
"""Calculate the volume and derivative of volume"""
"""with respect to crank angle""" 
"""Michael Sansoni"""
"""November 2013"""

import numpy

def volume_model(th, V_TDC, eps, compression_ratio):
    """Volume Model input th = crank angle (rads), volume_BDC = max cylinder volume (m^3), compression_ratio"""
    V=V_TDC*(1+(compression_ratio-1)/2.0*(1-numpy.cos(th)+1.0/eps*(1-(1-eps**2*numpy.sin(th)**2)**0.5)))
    DV=V_TDC*(compression_ratio-1)/2.0*(numpy.sin(th)+eps/2.0*numpy.sin(2*th)/numpy.sqrt(1-eps**2*numpy.sin(th)**2))
    return V, DV
