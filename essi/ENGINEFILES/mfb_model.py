"""Functions to return mass fraction burned"""
"""Uses Weibe model or cosine model"""
"""Michael Sansoni"""
"""November 2013"""

import numpy

def weibe_model(crank_angle, xr, crank_angle_ignition, burn_duration):
    """Combustion Model input crank_angle = crank angle, xr = x ratio, crank_angle_ignition = ignition start (rads), crank_angle_burn_duration = burn duration (rads)"""
    if crank_angle < crank_angle_ignition:                  #if angle is less than ignition start
        x = 0.0001                                              #x does not change
        dx = 0.0001                                              #as above
    else:
        x = (1 - numpy.exp(-5 * ((crank_angle - crank_angle_ignition)/burn_duration)**3))                       #x varies
        dx = (15/burn_duration) * (((crank_angle-crank_angle_ignition)/burn_duration)**2)*numpy.exp(-5*((crank_angle-crank_angle_ignition)/burn_duration)**3)        #dx varies
    if x<0.0001:
        x=0.0001
    if x>0.9999:
        x=0.9999
    return x, dx

def cosine_model(crank_angle, xr, crank_angle_ignition, burn_duration):
    """Combustion Model input crank_angle = crank angle, xr = x ratio, crank_angle_ignition = ignition start (rads), crank_angle_burn_duration = burn duration (rads)"""
    x=0.5*(1-numpy.cos(numpy.pi*(crank_angle-crank_angle_ignition)/burn_duration))
    dx=numpy.pi/2/burn_duration*numpy.sin(numpy.pi*(crank_angle-crank_angle_ignition)/burn_duration)
    if x<0.0001:
        x=0.0001
    if x>0.9999:
        x=0.9999
    return x, dx

def linear_model(crank_angle, xr, crank_angle_ignition, burn_duration):
    """Combustion Model input crank_angle = crank angle, xr = x ratio, crank_angle_ignition = ignition start (rads), crank_angle_burn_duration = burn duration (rads)"""
    x = (crank_angle - crank_angle_ignition)/burn_duration
    dx = 1.0
    if x<0.0001:
        x=0.0001
    if x>0.9999:
        x=0.9999
    return x, dx 
    
