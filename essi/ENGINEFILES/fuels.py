"""Fuel data"""
"""Data taken from Ferguson, Heywood and Rankin"""
"""Michael Sansoni"""
"""28th November 2013"""

def fuel_data(fuel):
    """Returns number of carbon, hydrogen, oxygen and nitrogen"""
    """molecules of fuel as well as fuel thermodynamic constants"""
    global alpha, beta, gamma, delta
    if fuel=="isooctane":
        alpha=8.0
        beta=18.0
        gamma=0.0
        delta=0.0
        fuel_data = [6.678E-1, 8.398E-2, -3.334E-5, 0, 0, -3.058E+4, 2.351E+1]

    elif fuel=="gasoline":
        alpha=7.0
        beta=17.0
        gamma=0.0
        delta=0.0
        fuel_data = [4.0652, 6.0977E-02, -1.8801E-05, 0, 0, -3.5880E+04, 15.45]

    elif fuel=="diesel":
        alpha=14.4
        beta=24.9
        gamma=0.0
        delta=0.0
        fuel_data = [7.9710, 1.1954E-01, -3.6858E-05, 0, 0, -1.9385E+04, -1.7879]

    elif fuel=="methanol":
        alpha=1.0
        beta=4.0
        gamma=1.0
        delta=0.0
        fuel_data = [1.779819, 1.262503E-02, -3.624890E-06, 0, 0, -2.525420E+04, 1.50884E+01]

    elif fuel=="methane":
        alpha=1
        beta=4
        gamma=0
        delta=0
        fuel_data = [1.971324, 7.871586E-03, -1.048592E-06, 0, 0, -9.930422E+03, 8.873728]

    elif fuel=="benzene":
        alpha=6
        beta=6
        gamma=0
        delta=0
        fuel_data = [-2.545087, 4.79554E-02, -2.030765E-05, 0, 0, 8.782234E+03, 3.348825E+01]
        
    else:
        """Use default fuel isooctane"""
        alpha=8.0
        beta=18.0
        gamma=0.0
        delta=0.0
        fuel_data = [6.678E-1, 8.398E-2, -3.334E-5, 0, 0, -3.058E+4, 2.351E+1]
        
    return alpha, beta, gamma, delta, fuel_data
