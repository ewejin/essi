"""Functions to return the valve area"""
"""and mass flow rates as a fucntion of"""
"""pressure and crank angle"""
"""R. Stone, Introduction to Internal Combustion Engines, 2012"""
"""This method uses a constant discharge coefficient"""
"""Equatiosn taken from Dawson, J. 1998"""
"""Michael Sansoni"""
"""January 2014"""

import numpy
import pylab

def valve_data(theta, vo, vc, Dp, Ds, lmax):
    Cd = 0.35
    if theta > vc:
        A = 0.0
        lift = 0.0
    else:
        lift = 0.5 * lmax * (1 - numpy.cos(2*numpy.pi*(theta-vo)/(vc-vo)))
        Cd_var = 190.47 * (lift/Dp)**4 - 143.13 * (lift/Dp)**3 + 31.248 * (lift/Dp)**2  - 2.5999 * (lift/Dp) + 0.6913
        Ap = Cd * 0.25 * numpy.pi * (Dp**2 - Ds**2) #Page 165 Ferguson 2001
        Av = Cd * numpy.pi * Dp * lift #Page 165 Ferguson 2001
        if Av < Ap:
            A = Av                                                                          #Dawson doctorial thesis
        else:
            A = Ap
    return A

def mass_flow(P0, T0, Pv, k, R, A, omega):
    if Pv > P0:
        mf = 0.0
    else:
        pc = (2/(k+1))**(k/(k-1)) #Choked flow pressure
        if (Pv/P0) > pc:
            mf = (A*k*P0/(R*T0)**0.5)/omega*(Pv/P0)**(1/k)*(((2*k)/(k-1))*(1-(Pv/P0)**((k-1)/k)))**0.5 #Dawson doctorial thesis
        else:
            mf = (A*k*P0/(R*T0)**0.5)/omega*(k)**0.5*(2/(k+1))**((k+1)/2*(k-1)) #Dawson doctorial thesis
    return mf
