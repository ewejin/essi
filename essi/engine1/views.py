from django.shortcuts import render
import matplotlib
matplotlib.use('Agg')
from matplotlib import pylab
from django.conf import settings

import numpy
from pylab import *
import PIL
import PIL.Image
import StringIO
import os
import time


"""Import plotting packages"""
from ENGINEFILES.ESSI_Simulator import *

"""Import plotting packages"""
import matplotlib.pyplot as plt

# Create your views here.


def simulate(request):
    if request.method == "POST":

        if request.POST.get('run') == "Run":
			# Start timer
			t = time.time()

			Engine1 = EngineSimulator()

			"Extracting parameters"
			phi = float(request.POST.get('phi'))
			xr = float(request.POST.get('xr'))
			P_int = float(request.POST.get('P_int'))
			T1 = float(request.POST.get('T1'))
			P_exh = float(request.POST.get('P_exh'))
			Tw = float(request.POST.get('Tw'))

			compression_ratio = request.POST.get('compression_ratio')
			e_rpm = request.POST.get('e_rpm')
			crank_angle_ignition = float(request.POST.get('crank_angle_ignition'))
			burn_duration = float(request.POST.get('burn_duration'))
			b = float(request.POST.get('b'))
			stroke = float(request.POST.get('stroke'))
			eps = float(request.POST.get('eps'))
			Cb = float(request.POST.get('Cb'))

			air = str(request.POST.get('air'))
			fuel = str(request.POST.get('fuel'))
			heat_transfer = str(request.POST.get('heat_transfer'))
			burn_model = str(request.POST.get('burn_model'))
			dissociation_model = str(request.POST.get('dissociation_model'))

			"""Changing the parameters"""
			"Initial condition"
			Engine1.phi = float(phi)
			Engine1.xr  = float(xr)  
			Engine1.P_int = float(P_int)*1000
			Engine1.T1 = float(T1)
			Engine1.P_exh = float(P_exh)*1000
			Engine1.Tw = float(Tw)

			"Engine parameters"
			Engine1.compression_ratio = float(compression_ratio)
			Engine1.e_rpm = float(e_rpm)
			Engine1.crank_angle_ignition = float(crank_angle_ignition)*numpy.pi/180
			Engine1.burn_duration = float(burn_duration)*numpy.pi/180
			Engine1.b = float(b)/1000
			Engine1.stroke = float(stroke)/1000
			Engine1.eps = float(eps)
			Engine1.Cb = float(Cb)

			"Simulation models"
			Engine1.air = str(request.POST.get('air'))
			Engine1.fuel = str(request.POST.get('fuel'))
			Engine1.heat_transfer = str(request.POST.get('heat_transfer'))
			Engine1.burn_model = str(request.POST.get('burn_model'))
			Engine1.dissociation_model = str(request.POST.get('dissociation_model'))

			"""Run Engine Simulation"""
			Engine1.run()

			# End timer
			elapsed = time.time() - t

			"""Extracting performance data"""
			imep = "%.2f" % round(Engine1.imep,2)
			sfc = "%.2f" % round(Engine1.sfc,2)
			power = "%.2f" % round(Engine1.power/1000,2)
			elapsed = "%.2f" % round(elapsed,2)
			context_dict = {
				# 't_eff':
				'imep': imep,
				# 'x':
				'sfc': sfc,
				'power': power,
				'elapsed': elapsed,
				'phi' : Engine1.phi,

				#Initial conditions				
				'phi': phi,
				'xr': xr,
				'P_int': P_int,
				'T1': T1,
				'P_exh': P_exh,
				'Tw': Tw,

				#Engine parameters
				'compression_ratio': compression_ratio,
				'e_rpm': e_rpm,
				'crank_angle_ignition': crank_angle_ignition,
				'burn_duration': burn_duration,
				'b': b,
				'stroke': stroke,
				'eps': eps,
				'Cb': Cb,

				#Simulating models
				'air': air,
				'fuel': fuel,
				'heat_transfer': heat_transfer,
				'burn_model': burn_model,
				'dissociation_model': dissociation_model,
			}

			"""Plotting graph"""
			"P-V"
			plt.plot(Engine1.vol_full, Engine1.pressure_full/1000, color = '#0A1F33', label='Engine 1')
			plt.xlabel('Volume [m$^{-3}$]')
			plt.ylabel('Pressure [kPa]')
								
			buffer = StringIO.StringIO()
			canvas = pylab.get_current_fig_manager().canvas
			canvas.draw()
			graphIMG = PIL.Image.fromstring("RGB", canvas.get_width_height(), canvas.tostring_rgb())
			graphIMG.save(buffer,"PNG")
			pylab.close()
			
			# saving image
			file_name = 'p_v.png'
			full_path = os.path.join(settings.IMAGE_PATH, file_name)
			with open(full_path, 'w') as f:
				f.write(buffer.getvalue())
				f.close()

			"Pressure"
			plt.plot(Engine1.crank_angle_full*50, Engine1.pressure_full/1000, color = '#0A1F33', label='Engine 1')
			plt.xlabel('Crank angle [deg]')
			plt.ylabel('Pressure [kPa]')
								
			buffer = StringIO.StringIO()
			canvas = pylab.get_current_fig_manager().canvas
			canvas.draw()
			graphIMG = PIL.Image.fromstring("RGB", canvas.get_width_height(), canvas.tostring_rgb())
			graphIMG.save(buffer,"PNG")
			pylab.close()
			
			# saving image
			file_name = 'pressure.png'
			full_path = os.path.join(settings.IMAGE_PATH, file_name)
			with open(full_path, 'w') as f:
				f.write(buffer.getvalue())
				f.close()

			"Temperature"
			fig = pylab.figure()
			ax = fig.add_subplot(111)
			ax.plot(Engine1.crank_angle_compcomb*50, Engine1.temperature_unburned_compcomb, color = '#0A1F33', label='Engine 1')
			ax.plot(Engine1.crank_angle_combexpexhint*50, Engine1.temperature_burned_combexpexhint, color = '#0A1F33', label='Engine 1')
			plt.xlabel('Crank angle [deg]')
			plt.ylabel('Temperature [K]')
								
			buffer = StringIO.StringIO()
			canvas = pylab.get_current_fig_manager().canvas
			canvas.draw()
			graphIMG = PIL.Image.fromstring("RGB", canvas.get_width_height(), canvas.tostring_rgb())
			graphIMG.save(buffer,"PNG")
			pylab.close()
			
			# saving image
			file_name = 'temperature.png'
			full_path = os.path.join(settings.IMAGE_PATH, file_name)
			with open(full_path, 'w') as f:
				f.write(buffer.getvalue())
				f.close()

			"Work"
			plt.plot(Engine1.crank_angle_full*50, Engine1.work_full, color = '#0A1F33', label='Engine 1')
			plt.xlabel('Crank angle [deg]')
			plt.ylabel('Work [J]')
								
			buffer = StringIO.StringIO()
			canvas = pylab.get_current_fig_manager().canvas
			canvas.draw()
			graphIMG = PIL.Image.fromstring("RGB", canvas.get_width_height(), canvas.tostring_rgb())
			graphIMG.save(buffer,"PNG")
			pylab.close()
			
			# saving image
			file_name = 'work.png'
			full_path = os.path.join(settings.IMAGE_PATH, file_name)
			with open(full_path, 'w') as f:
				f.write(buffer.getvalue())
				f.close()

			"Heat transfer"
			plt.plot(Engine1.crank_angle_half*50, Engine1.heat_transfer_half, color = '#0A1F33', label='Engine 1')
			plt.xlabel('Crank angle [deg]')
			plt.ylabel('Heat transfer [J]')
								
			buffer = StringIO.StringIO()
			canvas = pylab.get_current_fig_manager().canvas
			canvas.draw()
			graphIMG = PIL.Image.fromstring("RGB", canvas.get_width_height(), canvas.tostring_rgb())
			graphIMG.save(buffer,"PNG")
			pylab.close()
			
			# saving image
			file_name = 'heat.png'
			full_path = os.path.join(settings.IMAGE_PATH, file_name)
			with open(full_path, 'w') as f:
				f.write(buffer.getvalue())
				f.close()
			
			return render(request, 'engine1/parameters.html', context_dict)
        else:

			context_dict = {
				
			    #Initial conditions				
				'phi': 0.8,
				'xr': 0.1,
				'P_int': 100,
				'T1': 350,
				'P_exh': 150,
				'Tw': 420,

				#Engine parameters
				'compression_ratio': 10,
				'e_rpm': 2000,
				'crank_angle_ignition': -35,
				'burn_duration': 60,
				'b': 100,
				'stroke': 80,
				'eps': 0.25,
				'Cb': 0.8

				#Simulation models

			}

						
			return render(request, 'engine1/parameters.html', context_dict)
    else:

    	context_dict = {
				
			    #Initial conditions				
				'phi': 0.8,
				'xr': 0.1,
				'P_int': 100,
				'T1': 350,
				'P_exh': 150,
				'Tw': 420,

				#Engine parameters
				'compression_ratio': 10,
				'e_rpm': 2000,
				'crank_angle_ignition': -35,
				'burn_duration': 60,
				'b': 100,
				'stroke': 80,
				'eps': 0.25,
				'Cb': 0.8,

				#Simulation models
				'error_message': 'Please fill in all the fields',

			}

        
        return render(request, 'engine1/parameters.html', context_dict)
