from django.shortcuts import render


def intro(request):

    return render(request, 'intro.html')


def doc(request):

    return render(request, 'doc.html')


def about(request):

    return render(request, 'about.html')
