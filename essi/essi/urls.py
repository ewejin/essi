from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView

urlpatterns = patterns('',
    
    url(r'^admin/', include(admin.site.urls)),  # admin site
    url(r'^simulator/', include('engine1.urls')),  # redirects to engine1
    url(r'^$', 'essi.views.intro', name='intro'),  # intro page
    url(r'^doc/$', 'essi.views.doc', name='doc'),
    url(r'^about/$', 'essi.views.about', name='about'),
    )
