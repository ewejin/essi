# README #

### What is this repository for? ###

A continuation of developing a web application for a Spark Ignition engine using source code developed by M.Sansoni 

### How do I get set up? ###

1. Download the repository by clicking the download button on the left column.
2. Extract contents to a folder and taking note of its location.
3. Click start and type in 'cmd' without quotes in the serach box
4. Using the cmd, cd into the contents of the extracted folder.
5. Make sure virtualenv python package is installed on your machine otherwise run the command 'pip install virtualenv'
6. Installing required packages in current directory for virtualenv by running the command:
'virtualenv --no-site-packages env'. This command tells virtualenv to make current folder as the virtual environment folder and install the default packages in a folder named 'env'.
7. After installing them, run the command 'path/to/env ./install' to start virtual environment.
8. Virtual environment is initiated when '(env)' is displayed before the command line input.
9. Install the packages required as stated in the 'requirements.txt' using pip. Make sure pip install command is run as such: 'pip install "package"=="version no."' e.g 'pip install django==1.7'
10. You can check the packages installed in the virtual environment by running the command 'pip freeze'.
11. Once packages are installed, cd into 'path/to/new directory/essi/essi' and run 'python manage.py runserver', you have now started your own development server! Happy editing :)